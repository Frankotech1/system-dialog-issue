package com.example.systemdialogissue

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {

    private val CALL_PHONE_REQUEST_CODE = 10

    private var isDialogShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        run_ussd_btn.setOnClickListener {
            runUssd()
        }


    }

    private fun runUssd() {

        val str = "*556%23"

        val callIntent = Intent(Intent.ACTION_CALL,
            Uri.parse("tel: $str"))

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CALL_PHONE
            ) ==
            PackageManager.PERMISSION_GRANTED
        ) {
            startActivity(callIntent)

        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.CALL_PHONE),
                CALL_PHONE_REQUEST_CODE
            )
        }
        isDialogShown = true
    }

    override fun onResume() {
        super.onResume()

        if (isDialogShown) {
            Toast.makeText(this, "onResume() Called", Toast.LENGTH_LONG)
                .show()
        }
    }
}
